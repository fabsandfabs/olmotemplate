<?php

namespace App\Containers\Backoffice\Page\Controllers;

use Illuminate\Http\Request;

use App\Containers\Backoffice\Page\Models\Page;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersPreview;

class Controller extends BackController
{ 

    public function getPost(Request $request)
    {
        $attribute = Page::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Page::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Page::theAttributes();

        return $this->createNewPost($request, $attribute);
    }

    public function updatePost(Request $request)
    {
    	$attribute = Page::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }

    public function previewPost(Request $request)
    {
        $attribute = Page::theAttributes();
        $preview = true;

        return $this->serializePostUpdating($request, $attribute, $preview);
    }      

    public function previewPostDelete(Request $request)
    {
        $attribute = Page::theAttributes();
        $dataPost = HelpersData::idLangPost($request);  
        HelpersPreview::deletePreview($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }  

}
