<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_page', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            HelpersMigration::Page($table);
            // HelpersMigration::Default($table); Please, use this migration in case dalete HelpersMigration::Page($table);
            // HelpersMigration::Locale($table); Please, use this migration in case dalete HelpersMigration::Page($table) and want to add multilang-post;
            // $table->text('page_id_general')->nullable(false);
            // Content
            $table->text('title_txt_content')->nullable(false);
            $table->text('subtitle_editor_content')->nullable(false);
            $table->text('slidermain_slider_content')->nullable(false);
            $table->text('abstract_editor_content')->nullable(false);
            $table->text('slider_slider_content')->nullable(false);            
            $table->text('cover_filemanager_content')->nullable(false);
            $table->text('video_filemanager_content')->nullable(false);
            $table->text('cta_txt_content')->nullable(false);
            $table->text('content_editor_content')->nullable(false);
            // Visual Blocks
            $table->text('blocks_visual_blocks')->nullable(false);
            // SEO
            HelpersMigration::Seo($table);
            // Sitemap
            HelpersMigration::Sitemap($table); 
            // Menu
            HelpersMigration::Menu($table);
            // Ecommerce
            // HelpersMigration::EcommerceProduct('productitem', $table); If you have product items move this migration in to it and uncomment the line below
            // HelpersMigration::ProductItems($table);
            // HelpersMigration::EcommerceProductItems($table);
            // HelpersMigration::ECustomer($table);
            // $table->text('category_id_general')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_page');
    }
}
