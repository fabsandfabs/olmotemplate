<?php

namespace App\Containers\Backoffice\Page\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class Page extends OlmoMainModel
{

    public $timestamps = false;

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Page';

    public $table = 'olmo_page';
    
    public static function theAttributes(){
        return [
            // Those two keys are mandatory
            'table' => 'olmo_page',
            'required' => [],
            'route' => true,
            'duplicate' => true,
            'delete' => false,
            'cache' => false,
            'preview' => true,
            'menu' => true,
            // 'allmodelcachefrontend' => false,
            // 'order' => 'desc',
            // 'service-endpoint' => true,
            // 'orderlistget' => 'name_txt_general',
            // 'subcategory-folder-url' => 'category_id_general'
            // 'category-folder-url' => 'category_id_general'
            
            // Use this to setup to disable breadcrumbs
            'breadcrumbs' => false,

            // Use this to setup columns in the listing query
            'listing' => 'name_txt_general, slug_txt_general, lang_langs_general, template_id_general, enabled_is_general, id',

            // Use this setup for multilanguage posts
            'lang' => true,
            
            // Use this setup for posts with endpoint
            'requiredbackoffice' => [
                'name_txt_general',
                'slug_txt_general',
                'template_id_general'
            ],

            // // Some rules you could use
            // 'rules' => [
            // // This rule check if field is unique in the table
            //     [
            //         'type' => 'unique',
            //         'field' => 'email_email_general',
            //         'table' => 'olmo_user',
            //         'lang' => false,
            //         'errortxt' => 'Email duplicate'
            //     ],
            // // This rule check if field_one is equal to field_tow in the same table 
            //     [
            //         'type' => 'equal',
            //         'field_one' => 'password_pwd_general',
            //         'field_two' => 'confirmpassword_pwd_general',
            //         'errortxt' => 'Password not match'
            //     ],
            // // This rule check if another checkbox is true in the table
            //     [
            //          'type' => 'default',
            //          'field' => 'default_is_general',
            //          'table' => 'olmo_language',
            //          'lang' => false,
            //          'errortxt' => 'Just one default language can exists'
            //      ]                            
            // ], 
            // // This rule check if field_one has value condition_one then check if field_two has value condition_two in the request
            // [
            //     'type' => 'ifthen',
            //     'field_one' => 'default_is_general',
            //     'condition_one' => 'true',
            //     'field_two' => 'enabled_is_general',                    
            //     'condition_two' => 'true',
            //     'table' => 'olmo_language',
            //     'lang' => false,
            //     'errortxt' => "Can't be default if not enabled"
            // ]             

            // // Use this setup for posts with endpoint
            // 'rules' => [
            //     [
            //         'type' => 'unique',
            //         'field' => 'slug_txt_general',
            //         'table' => 'olmo_Testing',
            //         'errortxt' => 'Slug duplicate in page'
            //     ]               
            // ],  
            
            // // Show the importexport button in the listing page
            // 'importexport' => true,
            // 'csvstructure' => [
            //     // Specify in which column is located the unique value to make this work
            //     'id' => '0',
            //     'separator' => ',',
            //     // Map the csv column to the database field
            //     'mapping' =>[                    
            //         '0' => 'iso_txt_general',
            //         '1' => 'name_txt_general',
            //         '2' => 'enabled_is_general',
            //         '3' => 'email_txt_general'
            //     ]
            // ],                        

            // // Use this setup for Customer container 
            // 'required' => [
            //     'customerstatus_select_general',
            //     'email_email_general',
            //     'password_pwd_general',
            //     'confirmpassword_pwd_general',
            //     'name_txt_register',
            //     'surname_txt_register',
            //     'profession_txt_register',
            //     'company_txt_register',
            //     'country_txt_register',
            //     'vat_txt_register',
            //     'privacy_is_register',
            //     'terms_is_register'
            // ],
            // 'forms' => [
            //     'register' => [
            //          'groups' => ['register','email_email_general','password_pwd_general','confirmpassword_pwd_general'],
            //          'exclude' => [],
            //          'submit'  => true
            //     ],
            //     'profile' => [
            //         'groups' => ['register','profile'],
            //         'exclude' => ['privacy_is_register','terms_is_register'],
            //         'submit'  => true
            //     ]
            // ],
            // 'rules' => [
            //     [
            //         'type' => 'unique',
            //         'field' => 'email_email_general',
            //         'table' => 'olmo_customer',
            //         'lang' => false,
            //         'errortxt' => 'Email duplicate'
            //     ],
            //     [
            //         'type' => 'equal',
            //         'field_one' => 'password_pwd_general',
            //         'field_two' => 'confirmpassword_pwd_general',
            //         'errortxt' => 'Password not match'
            //     ]                
            // ],
            // 'cart' =>[
            //     'discountglobalfield' => 'vat_txt_register'
            // ],                   

        ];
    }    
}
