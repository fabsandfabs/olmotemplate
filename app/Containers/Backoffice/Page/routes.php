<?php

use App\Containers\Backoffice\Page\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/page', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the page post
Route::get('{lang}/page/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the page post
Route::put('{lang}/page/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Save preview the product post
Route::put('{lang}/page/{id}/preview', [Controller::class, 'previewPost'])->middleware(['auth:api']);

// Delete preview the product post
Route::post('{lang}/page/{id}/preview', [Controller::class, 'previewPostDelete'])->middleware(['auth:api']);

// Create the page post
Route::post('{lang}/page/create', [Controller::class, 'createPost'])->middleware(['auth:api']);