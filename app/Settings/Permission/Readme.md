# Permission setup guide

## Permission is a breacking feature which adds an extra-layer of management in your application, a specific USER-ROLE will be blocked to performs a specific action, or specific field either acceses to a whole model.

## As already said Olmo has three levels of permission:
- Actions
- Fields
- Menu

## Actions
###### Permissions are combine with Role, so to access in to the role permission you need to specify the exact role name as key like this:
```
{
    "admin": [],
    "editor": [],
    "publisher": []
}
```
###### then moving deeper, every object of the role array as two keys: model and actions. Model is the table model (es. olmo_page), actions the action you whant to deny.
```
{
    "admin": [],
    "editor": [
        {
            "model": "olmo_product",
            "actions": [
                "create",
                "delete"
            ]
        }  
    ],
    "publisher": []
}
```
###### Below the list of actions you can access:
- translate
- create (by default will hide the name_txt_general)
- delete
- duplicate
- visualblock-create
- visualblock-delete
- visualblock-modify

## Fields
###### Permissions are combine with Role, so to access in to the role permission you need to specify the exact role name as mentioned in Actions. Every object of the role array as two keys: model and fields. Model works as explained in Actions, instead fields is an array of the exact database column name:
```
{
    "model": "olmo_product",
    "fields": [
        "metatitle_txt_seo",
        "metatitle_txt_seo",
        "metadesc_txtarea_seo",
        "metakw_txt_seo",
        "ogtitle_txt_seo",
        "ogdesc_txtarea_seo",
        "ogimg_filemanager_seo",
    ]
}
```
###### Below the field types you can disabled if they are required in the backoffice
- _id_
- _multid_
- _select_
- _email_
- _is_
- _txt_
- _txtarea_
- _props_

## Menu
###### Permissions are combine with Role, so to access in to the role permission you need to specify the exact role name as mentioned above. This can be a little tricky, First, you need to specify the subheader, then if you are aware of the type specify it just in case the model has a sub-menu and you whant to hide only one label. Obviously, if you want to hide one label specify it in the array otherwise the whole model will be hidden.
```
{
    "editor": [
        {
            "subheader": "setting",
            "type": "dropdown",
            "items": ["users"]
        },
        {
            "subheader": "forms",
            "type": "linear",
            "items": []
        }                
    ]
}
```