# Dot setup guide

###### This is a basic example of how could be a dot table. This setup is also the one you find inside the table key
```
[
    "link_txt_content",
    "element_is_content",
    "brand_txt_content",
    "type_txt_content",
]
```

###### Some of them are mandatory, you haven to do nothing these are included by default
```
    "locale_hidden_content",
    "parentid_hidden_content",
    "model_hidden_content",
    "postid_hidden_content",
    "columnname_hidden_content",
    "name_txt_content",
    "left_num_content",
    "top_num_content",
    "description_txt_content",
    "image_filemanager_content",
```