<?php

namespace Database\Seeders;

use Olmo\Core\Loaders\OlmoSeederLoaderTrait;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    use OlmoSeederLoaderTrait;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->runLoadingSeeders();
    }
}
